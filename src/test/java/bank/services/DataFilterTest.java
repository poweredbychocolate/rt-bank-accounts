package bank.services;

import bank.model.Account;
import bank.model.Accounts;
import bank.model.BConfig;
import bank.model.BConfigBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * {@link DataFilter} tests
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-03
 */
class DataFilterTest {
    private Accounts accounts;

    /**
     * Before each.
     */
    @BeforeEach
    void beforeEach() {
        List<Account> list = new ArrayList<>();

        Account account = new Account();
        account.setIban("PL61109010140000071219812870");
        account.setName("name4");
        account.setCurrency("PLN");
        account.setBalance(0);
        account.setClosingDate(LocalDate.parse("2020-02-02"));
        list.add(account);

        account = new Account();
        account.setIban("PL61109010140000071219812875");
        account.setName("name1");
        account.setCurrency("PLN");
        account.setBalance(123.45);
        account.setClosingDate(LocalDate.parse("2031-06-10"));
        list.add(account);

        account = new Account();
        account.setIban("PL61109010140000071219812871");
        account.setName("name2");
        account.setCurrency("PLN");
        account.setBalance(85.00);
        account.setClosingDate(LocalDate.parse("2035-10-01"));
        list.add(account);

        account = new Account();
        account.setIban("PL61109010140000071219812872");
        account.setName("name3");
        account.setCurrency("USD");
        account.setBalance(1000000.00);
        account.setClosingDate(LocalDate.parse("2059-10-01"));
        list.add(account);

        account = new Account();
        account.setIban("PLL1109010140000071219812873");
        account.setName("name5");
        account.setCurrency("PLN");
        account.setBalance(999.00);
        account.setClosingDate(LocalDate.parse("2050-01-01"));
        list.add(account);

        account = new Account();
        account.setIban("PL61109010140000071219812874");
        account.setName("name6");
        account.setCurrency("PLN");
        account.setBalance(-100);
        account.setClosingDate(LocalDate.parse("2039-05-15"));
        list.add(account);

        account = new Account();
        account.setIban("PLL1109010140000071219812876");
        account.setName("name7");
        account.setCurrency("PLN");
        account.setBalance(1.00);
        account.setClosingDate(LocalDate.parse("2010-01-01"));
        list.add(account);

        accounts = new Accounts(list);
    }

    /**
     * After each.
     */
    @AfterEach
    void afterEach() {
        accounts.getAccountList().clear();
        accounts.setAccountList(null);
        accounts = null;
    }


    /**
     * Get.
     */
    @DisplayName("Test Data Filter")
    @Test
    void get() {
        assertEquals(7, accounts.getAccountList().size());
        Optional<BConfig> bConfig = new BConfigBuilder().mode(BConfig.MODE_DEFAULT).disableFilters().build();
        long notClosedCount = accounts.getAccountList().parallelStream()
                .filter(account -> account.getClosingDate().isAfter(LocalDate.now().minusDays(1))).count();

        assertTrue(bConfig.isPresent());
        Accounts tmp = DataFilter.get(accounts, bConfig.get());
        assertEquals(7, tmp.getAccountList().size());
        assertEquals("name1", tmp.getAccountList().get(0).getName());
        assertEquals("name3", tmp.getAccountList().get(2).getName());
        assertEquals("name7", tmp.getAccountList().get(6).getName());

        bConfig.get().setOnlyPLN(true);
        assertEquals(6, DataFilter.get(accounts, bConfig.get()).getAccountList().size());
        bConfig.get().setOnlyPLN(false);
        bConfig.get().setOnlyPositiveBalance(true);
        assertEquals(6, DataFilter.get(accounts, bConfig.get()).getAccountList().size());
        bConfig.get().setOnlyPositiveBalance(false);
        bConfig.get().setOnlyNotClosed(true);
        assertEquals(notClosedCount, DataFilter.get(accounts, bConfig.get()).getAccountList().size());
        bConfig.get().setOnlyNotClosed(false);
        bConfig.get().setOnlyCorrectIBAN(true);
        assertEquals(5, DataFilter.get(accounts, bConfig.get()).getAccountList().size());

        bConfig.get().setOnlyPLN(true);
        bConfig.get().setOnlyPositiveBalance(true);
        bConfig.get().setOnlyNotClosed(false);
        bConfig.get().setOnlyCorrectIBAN(false);
        assertEquals(5, DataFilter.get(accounts, bConfig.get()).getAccountList().size());

        bConfig.get().setOnlyPLN(true);
        bConfig.get().setOnlyPositiveBalance(true);
        bConfig.get().setOnlyNotClosed(true);
        bConfig.get().setOnlyCorrectIBAN(true);
        tmp = DataFilter.get(accounts, bConfig.get());
        long count = tmp.getAccountList().parallelStream().filter(account -> account.getBalance() < 0).count();
        assertEquals(0, count);
        count = tmp.getAccountList().parallelStream().filter(account -> account.getCurrency().equals("USD")).count();
        assertEquals(0, count);
        count = tmp.getAccountList().parallelStream().filter(account -> account.getIban().startsWith("PLL")).count();
        assertEquals(0, count);
        count = tmp.getAccountList().parallelStream().filter(account -> account.getClosingDate().isBefore(LocalDate.now())).count();
        assertEquals(0, count);
    }
}