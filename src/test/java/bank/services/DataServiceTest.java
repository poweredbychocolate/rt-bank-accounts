package bank.services;

import bank.model.Account;
import bank.model.Accounts;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * {@link DataService} tests.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-02
 */
class DataServiceTest {
    private static List<Account> accountList;

    @BeforeAll
    static void init() {
        Account account = new Account();
        account.setBalance(1250.65);
        account.setClosingDate(LocalDate.parse("2030-02-23"));
        account.setCurrency("PLN");
        account.setName("Single Test");
        account.setIban("PL61109010140000071219812871");
        accountList = new ArrayList<>();
        accountList.add(account);

        account = new Account();
        account.setBalance(150.21);
        account.setClosingDate(LocalDate.parse("2021-11-04"));
        account.setCurrency("PLN");
        account.setName("Test 1");
        account.setIban("PLL1109010140000071219812873");
        accountList.add(account);

        account = new Account();
        account.setBalance(-250.65);
        account.setClosingDate(LocalDate.parse("2024-08-22"));
        account.setCurrency("PLN");
        account.setName("Test 2");
        account.setIban("PL61109010340070071219812671");
        accountList.add(account);

        account = new Account();
        account.setBalance(2.12);
        account.setClosingDate(LocalDate.parse("2018-06-22"));
        account.setCurrency("PLN");
        account.setName("Big test");
        account.setIban("PL61109010140000071219812870");
        accountList.add(account);
    }

    @DisplayName("Test Single Account Save")
    @Test
    void saveAccount() throws JAXBException {
        DataService dataService = new DataService(Account.class);
        assertNotNull(dataService);
        assertTrue(dataService.save(accountList.get(0), Paths.get("src/test/resources/account-output.xml")));
    }

    @DisplayName("Test Accounts List Save")
    @Test
    void saveAccountList() throws JAXBException {
        Accounts accounts = new Accounts(accountList);
        DataService dataService = new DataService(Accounts.class);
        assertNotNull(dataService);
        assertTrue(dataService.save(accounts, Paths.get("src/test/resources/a-list-output.xml")));
    }

    @DisplayName("Test Single Account Read")
    @Test
    void loadAccount() throws JAXBException {
        DataService dataService = new DataService(Account.class);
        assertNotNull(dataService);
        Optional<Account> optional = dataService.load(Paths.get("src/test/resources/account-input.xml"));
        assertNotNull(optional);
        assertTrue(optional.isPresent());
        Account account = optional.get();
        assertEquals(account, accountList.get(0));
        assertNotEquals(account, accountList.get(1));
        assertNotEquals(account, accountList.get(2));
        assertNotEquals(account, accountList.get(3));
    }

    @DisplayName("Test Accounts Load")
    @Test
    void loadAccounts() throws JAXBException {
        DataService dataService = new DataService(Accounts.class);
        assertNotNull(dataService);
        Optional<Accounts> optional = dataService.load(Paths.get("src/test/resources/input.xml"));
        assertNotNull(optional);
        assertTrue(optional.isPresent());
        List<Account> accounts = optional.get().getAccountList();
        assertEquals(4, accounts.size());
        assertEquals(accounts.get(0), accountList.get(0));
        assertEquals(accounts.get(1), accountList.get(1));
        assertEquals(accounts.get(2), accountList.get(2));
        assertEquals(accounts.get(3), accountList.get(3));
        assertEquals(accounts.get(0).getIban(), accountList.get(0).getIban());
        assertEquals(accounts.get(2).getIban(), accountList.get(2).getIban());
    }
}