package bank.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


/**
 * {@link BConfigBuilder} and {@link BConfig} test.
 */
class BConfigBuilderTest {

    @DisplayName("Test return not present BConfig")
    @Test
    void emptyConfig() {
        Optional<BConfig> bConfig = new BConfigBuilder().build();
        assertNotNull(bConfig);
        assertFalse(bConfig.isPresent());
        bConfig = new BConfigBuilder().mode("test").build();
        assertNotNull(bConfig);
        assertFalse(bConfig.isPresent());
        bConfig = new BConfigBuilder().mode("test").fileName("i.txt", "o.txt")
                .filters(true, false, false, true).build();
        assertNotNull(bConfig);
        assertFalse(bConfig.isPresent());
    }


    @DisplayName("Test return present BConfig")
    @Test
    void notEmptyConfig() {
        Optional<BConfig> bConfig = new BConfigBuilder().mode(BConfig.MODE_DEFAULT).build();
        assertNotNull(bConfig);
        assertTrue(bConfig.isPresent());
        bConfig = new BConfigBuilder().mode(BConfig.MODE_DEFAULT).fileName("i.txt", "o.txt")
                .filters(true, false, false, true).build();
        assertNotNull(bConfig);
        assertTrue(bConfig.isPresent());
    }


    @DisplayName("Test BConfig param")
    @Test
    void testBConfig() {
        Optional<BConfig> bConfig = new BConfigBuilder().mode(BConfig.MODE_DEFAULT).build();
        assertNotNull(bConfig);
        assertTrue(bConfig.isPresent());
        BConfig config = bConfig.get();
        assertEquals(config.getMode(), BConfig.MODE_DEFAULT);
        assertEquals(config.getInputFileName(), BConfig.DEFAULT_INPUT_FILE_NAME);
        assertEquals(config.getOutputFileName(), BConfig.DEFAULT_OUTPUT_FILE_NAME);
        assertFalse(config.isOnlyCorrectIBAN());
        assertFalse(config.isOnlyNotClosed());
        assertFalse(config.isOnlyPLN());
        assertFalse(config.isOnlyPositiveBalance());

        bConfig = new BConfigBuilder().mode(BConfig.MODE_DEFAULT).disableFilters().build();
        assertNotNull(bConfig);
        assertTrue(bConfig.isPresent());
        config = bConfig.get();
        assertEquals(config.getMode(), BConfig.MODE_DEFAULT);
        assertEquals(config.getInputFileName(), BConfig.DEFAULT_INPUT_FILE_NAME);
        assertEquals(config.getOutputFileName(), BConfig.DEFAULT_OUTPUT_FILE_NAME);
        assertFalse(config.isOnlyCorrectIBAN());
        assertFalse(config.isOnlyNotClosed());
        assertFalse(config.isOnlyPLN());
        assertFalse(config.isOnlyPositiveBalance());

        bConfig = new BConfigBuilder().mode(BConfig.MODE_DEFAULT).enableFilters().build();
        assertNotNull(bConfig);
        assertTrue(bConfig.isPresent());
        config = bConfig.get();
        assertEquals(config.getMode(), BConfig.MODE_DEFAULT);
        assertEquals(config.getInputFileName(), BConfig.DEFAULT_INPUT_FILE_NAME);
        assertEquals(config.getOutputFileName(), BConfig.DEFAULT_OUTPUT_FILE_NAME);
        assertTrue(config.isOnlyCorrectIBAN());
        assertTrue(config.isOnlyNotClosed());
        assertTrue(config.isOnlyPLN());
        assertTrue(config.isOnlyPositiveBalance());

        String i = "i.txt";
        String o = "o.txt";
        bConfig = new BConfigBuilder().mode(BConfig.MODE_DEFAULT).fileName(i, o)
                .filters(true, false, false, true).build();
        assertNotNull(bConfig);
        assertTrue(bConfig.isPresent());
        config = bConfig.get();
        assertEquals(config.getMode(), BConfig.MODE_DEFAULT);
        assertEquals(config.getInputFileName(), i);
        assertEquals(config.getOutputFileName(), o);
        assertTrue(config.isOnlyCorrectIBAN());
        assertFalse(config.isOnlyNotClosed());
        assertTrue(config.isOnlyPLN());
        assertFalse(config.isOnlyPositiveBalance());
    }
}