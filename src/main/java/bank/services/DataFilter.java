package bank.services;

import bank.model.Account;
import bank.model.Accounts;
import bank.model.BConfig;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * {@link Accounts} filter.
 * @author Dawid
 * @since 2020-02-03
 * @version 1
 */
public class DataFilter {
    private static final int IBAN_LENGTH = 28;

    /**
     * Filter {@link Account} using {@link BConfig} as filter setting
     *
     * @param accounts the accounts
     * @param bConfig  the b config
     * @return the accounts
     */
    public static Accounts get(Accounts accounts, BConfig bConfig) {
        Predicate<Account> accountPredicate = getPredicate(bConfig);
        List<Account> accountsList;
        if (accountPredicate != null) {
            accountsList = accounts.getAccountList().parallelStream().filter(accountPredicate).collect(Collectors.toList());
        } else {
            accountsList = accounts.getAccountList();
        }
        return new Accounts(accountsList.parallelStream().sorted(Comparator.comparing(Account::getName)).collect(Collectors.toList()));
    }
    private static Predicate<Account> getPredicate(BConfig bConfig){
        Predicate<Account> predicate = null;
        if (bConfig.isOnlyPLN()) {
            predicate = account -> account.getCurrency().equals("PLN");
        }
        if (bConfig.isOnlyPositiveBalance()) {
            Predicate<Account> tmp = account -> account.getBalance() >= 0;
            if (predicate != null) {
                predicate = predicate.and(tmp);
            } else {
                predicate = tmp;
            }
        }
        if (bConfig.isOnlyNotClosed()) {
            Predicate<Account> tmp = account -> account.getClosingDate().isAfter(LocalDate.now().minusDays(1));
            if (predicate != null) {
                predicate = predicate.and(tmp);
            } else {
                predicate = tmp;
            }
        }
        if (bConfig.isOnlyCorrectIBAN()) {
            Predicate<Account> tmp = account -> account.getIban().length() == IBAN_LENGTH && account.getIban().startsWith("PL") && account.getIban().substring(2).chars().allMatch(Character::isDigit);
            if (predicate != null) {
                predicate = predicate.and(tmp);
            } else {
                predicate = tmp;
            }
        }
        return predicate;
    }
}
