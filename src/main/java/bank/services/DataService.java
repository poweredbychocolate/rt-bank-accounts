package bank.services;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.nio.file.Path;
import java.util.Optional;

/**
 * Data service read and save xml files.
 */
public class DataService {
    private Marshaller marshaller;
    private Unmarshaller unmarshaller;

    /**
     * Instantiates a new Data service.
     *
     * @param clazz the clazz
     * @throws JAXBException the jaxb exception
     */
    public DataService(Class clazz) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
        this.marshaller= jaxbContext.createMarshaller();
        this.marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
        this.unmarshaller = jaxbContext.createUnmarshaller();
    }

    /**
     * Save object to XML file.
     *
     * @param <T>    the type parameter
     * @param toSave the to save
     * @param path   the path
     * @return the boolean
     */
    public <T> boolean save(T toSave, Path path){
        try {
            marshaller.marshal(toSave,path.toFile());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * Load object from XML file as optional.
     *
     * @param <T>  the type parameter
     * @param path the path
     * @return the optional
     */
    public <T> Optional<T> load(Path path){
        try {
            T object= (T) unmarshaller.unmarshal(path.toFile());
            return Optional.ofNullable(object);
        } catch (JAXBException e) {
            return Optional.empty();
        }

    }
}
