package bank.ui;

import bank.model.Accounts;
import bank.model.BConfig;
import bank.services.DataFilter;
import bank.services.DataService;

import javax.xml.bind.JAXBException;
import java.nio.file.Paths;
import java.util.Optional;

/**
 * Default user interface.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-03
 */
public class DefaultUI implements BankUI {
    private BConfig bConfig;

    /**
     * Instantiates a new Default ui.
     *
     * @param bConfig the b config
     */
    public DefaultUI(BConfig bConfig) {
        this.bConfig = bConfig;
    }

    @Override
    public void start() {
        try {
            DataService dataService = new DataService(Accounts.class);
            System.out.println("[Data loading...]");
            Optional<Accounts> accounts = dataService.load(Paths.get(bConfig.getInputFileName()));
            if (accounts.isPresent()) {
                System.out.println("[Data filtering...]");
                Accounts tmp = DataFilter.get(accounts.get(), bConfig);
                System.out.println("[Generating output file...]");
                dataService.save(tmp, Paths.get(bConfig.getOutputFileName()));
                System.out.println("[Done]");
            } else {
                System.out.println("[Cannot read xml file]");
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
