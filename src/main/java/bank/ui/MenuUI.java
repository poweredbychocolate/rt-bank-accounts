package bank.ui;

import bank.model.Accounts;
import bank.model.BConfig;
import bank.services.DataFilter;
import bank.services.DataService;

import javax.xml.bind.JAXBException;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Scanner;

/**
 * User interface with setting customization.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-03
 */
public class MenuUI implements BankUI {
    private BConfig bConfig;

    /**
     * Instantiates a new Menu ui.
     *
     * @param bConfig the b config
     */
    public MenuUI(BConfig bConfig) {
        this.bConfig = bConfig;
    }

    @Override
    public void start() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter input file path [blank to skip] :");
        String menu = scanner.nextLine();
        if (!menu.isEmpty()) {
            bConfig.setInputFileName(menu);
        }
        System.out.println("Enter output file path [blank to skip] :");
        menu = scanner.nextLine();
        if (!menu.isEmpty()) {
            bConfig.setOutputFileName(menu);
        }
        System.out.println("Only PLN currency [Y/N] :");
        menu = scanner.nextLine();
        if (menu.equalsIgnoreCase("y")) {
            bConfig.setOnlyPLN(true);
        } else {
            bConfig.setOnlyPLN(false);
        }
        System.out.println("Only positive balance [Y/N] :");
        menu = scanner.nextLine();
        if (menu.equalsIgnoreCase("y")) {
            bConfig.setOnlyPositiveBalance(true);
        } else {
            bConfig.setOnlyPositiveBalance(false);
        }
        System.out.println("Only not closed account [Y/N] :");
        menu = scanner.nextLine();
        if (menu.equalsIgnoreCase("y")) {
            bConfig.setOnlyNotClosed(true);
        } else {
            bConfig.setOnlyNotClosed(false);
        }
        System.out.println("Only correct PLN IBAN [Y/N] :");
        menu = scanner.nextLine();
        if (menu.equalsIgnoreCase("y")) {
            bConfig.setOnlyCorrectIBAN(true);
        } else {
            bConfig.setOnlyCorrectIBAN(false);
        }
        try {
            DataService dataService = new DataService(Accounts.class);
            System.out.println("[Data loading...]");
            Optional<Accounts> accounts = dataService.load(Paths.get(bConfig.getInputFileName()));
            if (accounts.isPresent()) {
                System.out.println("[Data filtering...]");
                Accounts tmp = DataFilter.get(accounts.get(), bConfig);
                System.out.println("[Generating output file...]");
                dataService.save(tmp, Paths.get(bConfig.getOutputFileName()));
                System.out.println("[Done]");
            } else {
                System.out.println("[Cannot read xml file]");
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
