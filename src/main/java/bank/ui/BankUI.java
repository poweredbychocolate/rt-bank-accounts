package bank.ui;

/**
 * The interface Bank ui.
 * @author Dawid
 * @since 2020-02-03
 * @version 1
 */
public interface BankUI {
    /**
     * Start user interface.
     */
    public void start();
}
