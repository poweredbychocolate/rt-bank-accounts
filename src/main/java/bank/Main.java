package bank;

import bank.model.BConfig;
import bank.model.BConfigBuilder;
import bank.ui.BankUI;
import bank.ui.DefaultUI;
import bank.ui.MenuUI;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        BConfigBuilder bConfigBuilder = new BConfigBuilder();
        if (args.length == 0) {
            bConfigBuilder.enableFilters().mode(BConfig.MODE_DEFAULT);
        } else if (args.length == 1) {
            bConfigBuilder.mode(args[0]).disableFilters();
        } else if (args.length == 2) {
            bConfigBuilder.mode(BConfig.MODE_DEFAULT).fileName(args[0], args[1]).enableFilters();
        } else if (args.length == 3) {
            bConfigBuilder.mode(args[0]).fileName(args[1], args[2]).enableFilters();
        }
        Optional<BConfig> bConfig = bConfigBuilder.build();
        if (bConfig.isPresent()) {
            BankUI bankUI = null;
            if (bConfig.get().getMode().equals(BConfig.MODE_DEFAULT)) {
                bankUI = new DefaultUI(bConfig.get());
            } else if (bConfig.get().getMode().equals(BConfig.MODE_MENU)) {
                bankUI = new MenuUI(bConfig.get());
            }
            if (bankUI != null) {
                bankUI.start();
            }

        } else {
            System.out.println("Unknown configuration");
        }

    }
}
