package bank.model;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;

/**
 * {@link LocalDate} converter for closingDate attribute
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-02
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
    @Override
    public LocalDate unmarshal(String string)  {
        return LocalDate.parse(string);
    }

    @Override
    public String marshal(LocalDate date)  {
        return date.toString();
    }
}
