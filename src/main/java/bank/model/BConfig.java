package bank.model;

import java.util.Objects;

/**
 *  BConfig main app settings.
 * @author Dawid
 * @since 2020-02-02
 * @version 1
 */
public class BConfig {
    public static final String DEFAULT_INPUT_FILE_NAME= "input.xml";
    public static final String DEFAULT_OUTPUT_FILE_NAME= "output.xml";
    public static final String MODE_DEFAULT= "default";
    public static final String MODE_MENU= "menu";

    private String inputFileName;
    private String outputFileName;
    private String mode;
    private boolean onlyPLN;
    private boolean onlyPositiveBalance;
    private boolean onlyNotClosed;
    private boolean onlyCorrectIBAN;

    /**
     * Instantiates a new B config.
     */
    BConfig() {
        this.inputFileName= DEFAULT_INPUT_FILE_NAME;
        this.outputFileName=DEFAULT_OUTPUT_FILE_NAME;
        this.mode=MODE_DEFAULT;
    }

    /**
     * Gets input file name.
     *
     * @return the input file name
     */
    public String getInputFileName() {
        return inputFileName;
    }

    /**
     * Sets input file name.
     *
     * @param inputFileName the input file name
     */
    public void setInputFileName(String inputFileName) {
        this.inputFileName = inputFileName;
    }

    /**
     * Gets output file name.
     *
     * @return the output file name
     */
    public String getOutputFileName() {
        return outputFileName;
    }

    /**
     * Sets output file name.
     *
     * @param outputFileName the output file name
     */
    public void setOutputFileName(String outputFileName) {
        this.outputFileName = outputFileName;
    }

    /**
     * Gets mode.
     *
     * @return the mode
     */
    public String getMode() {
        return mode;
    }

    /**
     * Sets mode.
     *
     * @param mode the mode
     */
   void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Is only pln boolean.
     *
     * @return the boolean
     */
    public boolean isOnlyPLN() {
        return onlyPLN;
    }

    /**
     * Sets only pln.
     *
     * @param onlyPLN the only pln
     */
    public void setOnlyPLN(boolean onlyPLN) {
        this.onlyPLN = onlyPLN;
    }

    /**
     * Is only positive balance boolean.
     *
     * @return the boolean
     */
    public boolean isOnlyPositiveBalance() {
        return onlyPositiveBalance;
    }

    /**
     * Sets only positive balance.
     *
     * @param onlyPositiveBalance the only positive balance
     */
    public void setOnlyPositiveBalance(boolean onlyPositiveBalance) {
        this.onlyPositiveBalance = onlyPositiveBalance;
    }

    /**
     * Is only not closed boolean.
     *
     * @return the boolean
     */
    public boolean isOnlyNotClosed() {
        return onlyNotClosed;
    }

    /**
     * Sets only not closed.
     *
     * @param onlyNotClosed the only not closed
     */
    public void setOnlyNotClosed(boolean onlyNotClosed) {
        this.onlyNotClosed = onlyNotClosed;
    }

    /**
     * Is only correct iban boolean.
     *
     * @return the boolean
     */
    public boolean isOnlyCorrectIBAN() {
        return onlyCorrectIBAN;
    }

    /**
     * Sets only correct iban.
     *
     * @param onlyCorrectIBAN the only correct iban
     */
    public void setOnlyCorrectIBAN(boolean onlyCorrectIBAN) {
        this.onlyCorrectIBAN = onlyCorrectIBAN;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BConfig bConfig = (BConfig) o;
        return onlyPLN == bConfig.onlyPLN &&
                onlyPositiveBalance == bConfig.onlyPositiveBalance &&
                onlyNotClosed == bConfig.onlyNotClosed &&
                onlyCorrectIBAN == bConfig.onlyCorrectIBAN &&
                Objects.equals(inputFileName, bConfig.inputFileName) &&
                Objects.equals(outputFileName, bConfig.outputFileName) &&
                Objects.equals(mode, bConfig.mode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(inputFileName, outputFileName, mode, onlyPLN, onlyPositiveBalance, onlyNotClosed, onlyCorrectIBAN);
    }

    @Override
    public String toString() {
        return "BConfig{" +
                "inputFileName='" + inputFileName + '\'' +
                ", outputFileName='" + outputFileName + '\'' +
                ", mode='" + mode + '\'' +
                ", onlyPLN=" + onlyPLN +
                ", onlyPositiveBalance=" + onlyPositiveBalance +
                ", onlyNotClosed=" + onlyNotClosed +
                ", onlyCorrectIBAN=" + onlyCorrectIBAN +
                '}';
    }
}
