package bank.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Xml Accounts model.
 *
 * @author Dawid
 * @version 1
 * @since 2020-02-02
 */
@XmlRootElement(name = "accounts")
public class Accounts {
    private List<Account> accountList;

    /**
     * Instantiates a new Accounts.
     */
    public Accounts() {
    }

    /**
     * Instantiates a new Accounts.
     *
     * @param accountList the account list
     */
    public Accounts(List<Account> accountList) {
        this.accountList = accountList;
    }

    /**
     * Gets account list.
     *
     * @return the account list
     */
    public List<Account> getAccountList() {
        return accountList;
    }

    /**
     * Sets account list.
     *
     * @param accountList the account list
     */
    @XmlElement(name = "account")
    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

}
