package bank.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.util.Objects;

/**
 * XML Account model.
 *
 * @author Dawid
 * @version 1
 * @since 2020 -02-02
 */
@XmlType(propOrder = {"name", "currency", "balance", "closingDate"})
@XmlRootElement(name = "account")
public class Account {
    private String iban;
    private String name;
    private String currency;
    private double balance;
    private LocalDate closingDate;

    /**
     * Instantiates a new Account.
     */
    public Account() {
    }

    /**
     * Gets iban.
     *
     * @return the iban
     */
    public String getIban() {
        return iban;
    }

    /**
     * Sets iban.
     *
     * @param iban the iban
     */
    @XmlAttribute(name = "iban", required = true)
    public void setIban(String iban) {
        this.iban = iban;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    @XmlElement(name = "name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets currency.
     *
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets currency.
     *
     * @param currency the currency
     */
    @XmlElement(name = "currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Gets balance.
     *
     * @return the balance
     */
    public double getBalance() {
        return balance;
    }

    /**
     * Sets balance.
     *
     * @param balance the balance
     */
    @XmlElement(name = "balance")
    public void setBalance(double balance) {
        this.balance = balance;
    }

    /**
     * Gets closing date.
     *
     * @return the closing date
     */
    public LocalDate getClosingDate() {
        return closingDate;
    }

    /**
     * Sets closing date.
     *
     * @param closingDate the closing date
     */
    @XmlElement(name = "closingDate")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setClosingDate(LocalDate closingDate) {
        this.closingDate = closingDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Double.compare(account.balance, balance) == 0 &&
                Objects.equals(iban, account.iban) &&
                Objects.equals(name, account.name) &&
                Objects.equals(currency, account.currency) &&
                Objects.equals(closingDate, account.closingDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iban, name, currency, balance, closingDate);
    }
}
