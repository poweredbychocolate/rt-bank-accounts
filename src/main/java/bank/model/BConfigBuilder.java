package bank.model;

import java.util.Optional;

/**
 * Settings builder for {@link BConfig}.
 *
 * @author Dawid
 * @version 1
 * @since 2020 -02-02
 */
public class BConfigBuilder {

    private String inputFileName =null;
    private String outputFileName =null;
    private String mode;
    private boolean onlyPLN;
    private boolean onlyPositiveBalance;
    private boolean onlyNotClosed;
    private boolean onlyCorrectIBAN;

    /**
     * Instantiates a new B config builder.
     */
    public BConfigBuilder() {
    }

    /**
     * Mode b config builder.
     *
     * @param mode the mode
     * @return the b config builder
     */
    public BConfigBuilder mode(String mode) {
        this.mode = mode;
        return this;
    }

    /**
     * File name b config builder.
     *
     * @param inputFileName  the input file name
     * @param outputFileName the output file name
     * @return the b config builder
     */
    public BConfigBuilder fileName(String inputFileName, String outputFileName) {
        if (inputFileName != null) this.inputFileName = inputFileName;
        if (outputFileName != null) this.outputFileName = outputFileName;
        return this;
    }

    /**
     * Filters b config builder.
     *
     * @param onlyPLN             the only pln
     * @param onlyPositiveBalance the only positive balance
     * @param onlyNotClosed       the only not closed
     * @param onlyCorrectIBAN     the only correct iban
     * @return the b config builder
     */
    public BConfigBuilder filters(boolean onlyPLN, boolean onlyPositiveBalance, boolean onlyNotClosed, boolean onlyCorrectIBAN) {
        this.onlyPLN = onlyPLN;
        this.onlyPositiveBalance = onlyPositiveBalance;
        this.onlyNotClosed = onlyNotClosed;
        this.onlyCorrectIBAN = onlyCorrectIBAN;
        return this;
    }

    /**
     * Enable filters b config builder.
     *
     * @return the b config builder
     */
    public BConfigBuilder enableFilters() {
        this.onlyPLN = true;
        this.onlyPositiveBalance = true;
        this.onlyNotClosed = true;
        this.onlyCorrectIBAN = true;
        return this;
    }

    /**
     * Disable filters b config builder.
     *
     * @return the b config builder
     */
    public BConfigBuilder disableFilters() {
        this.onlyPLN = false;
        this.onlyPositiveBalance = false;
        this.onlyNotClosed = false;
        this.onlyCorrectIBAN = false;
        return this;
    }

    private boolean validMode() {
        return this.mode!=null&&(this.mode.equalsIgnoreCase(BConfig.MODE_DEFAULT)||this.mode.equalsIgnoreCase(BConfig.MODE_MENU)) ;

    }

    /**
     * Build optional.
     *
     * @return the optional
     */
    public Optional<BConfig> build() {
        Optional<BConfig> optionalBConfig;
        if (validMode()){
            BConfig bConfig = new BConfig();
            bConfig.setMode(this.mode);
            if(this.inputFileName!=null)bConfig.setInputFileName(this.inputFileName);
            if(this.outputFileName!=null)bConfig.setOutputFileName(this.outputFileName);
            bConfig.setOnlyPLN(this.onlyPLN);
            bConfig.setOnlyPositiveBalance(this.onlyPositiveBalance);
            bConfig.setOnlyNotClosed(this.onlyNotClosed);
            bConfig.setOnlyCorrectIBAN(this.onlyCorrectIBAN);
            optionalBConfig = Optional.of(bConfig);
        }else{
            optionalBConfig = Optional.empty();
        }
        return optionalBConfig;
    }
}
